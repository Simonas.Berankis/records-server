# Instructions

To run simulation:

```bash
npm run start:http
```

If you want to work on MQTT extra parts

```bash
npm run start:mqtt
```

## Configuration

All of the simulation configuration can be controlled with env variables or changed in index.ts

Default HTTP server location - localhost:3000

Default MQTT server location - localhost:1883

## Task

Create a simple NodeJS server to save data from simulated embedded system (we will provide the source code for it)
and return the latest record by device ID using a HTTP endpoint. Your task is to use technologies that you think will solve
the problem of communicating with the device, storing and querying the data. The solution should be easy to deploy on a
virtual machine and configured using IaC pattern (Infrastructure as Code). We hope that you are familiar with AWS (or
any other similar cloud provider) and already have your own account.
The end result is a git repository that we can easily deploy, test it against the simulated device and call HTTP endpoint to
check what is the latest record of a device.

### Requirements

* For code use Node.js, Typescript, Git.
* Server supports device HTTP traffic.
* Choose a database and server framework that helps in solving the problem.
* No UI is needed, server endpoints can be tested with Postman.
* Prepare IaC which automatically deploys infrastructure (choose any tool like Ansible, Terraform, AWS CDK).

### Extra Mile

* Organize server, implement architectural pattern(s) or use tools which allows to add other protocols easily,
simplify data flow between devices and UI.
* Server supports device MQTT traffic.
* Create a HTTP POST endpoint which saves the payload in the database and sends it to a device
* Device can send messages using TLS (HTTPS and MQTTS)
