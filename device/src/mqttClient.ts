import { connect, MqttClient } from 'mqtt'
import cbor from 'cbor'

import { IMqttConfig, IMqttPubAction, IMqttSubAction } from './config'
import { ActionType } from '.'

export async function startMqttClient(config: IMqttConfig) {
  const c = await mqttClient(config)

  c.on('message', (topic: string, payload: any) => {
    handleMessage(payload, topic)
  })

  for (const action of config.actions) {
    if (action.type === ActionType.MqttSub) {
      handleSub(c, action as IMqttSubAction)
    }
    if (action.type === ActionType.MqttPub) {
      handlePub(c, action as IMqttPubAction)
    }
  }
}

function mqttClient(config: any): Promise<MqttClient> {
  const client = connect(`${config.scheme}://${config.hostName}`, {
    clean: false,
    clientId: config.clientId,
    protocol: config.scheme,
    port: 1883,

    rejectUnauthorized: false,
    username: config.username,
    password: config.password,
  })

  return new Promise((resolve) => {
    client.on('connect', function () {
      console.log('connect')
      resolve(client)
    })
    client.on('close', function () {
      console.log('close')
    })
    client.on('reconnect', function () {
      console.log('reconnect')
    })
    client.on('offline', function () {
      console.log('offline')
    })
    client.on('error', function (error: any) {
      console.log('error', error)
    })
  })
}

function handleMessage(payload: any, topic: string) {
  const buffer =
    payload instanceof Buffer ? payload : Buffer.from(payload, 'hex')
  const response = cbor.decodeFirstSync(buffer)

  console.log(`Topic[${topic}]: ${JSON.stringify(response)}`)
}

function handlePub(client: MqttClient, action: IMqttPubAction) {
  const maxCount = action.count || 10
  let publishCounter = 0

  const interval = setInterval(function () {
    console.log(`Publishing [${publishCounter}] to:`, action.topic)

    const data = action?.payload
      ? Buffer.from(cbor.encode(action?.payload()).toString('hex'), 'hex')
      : Buffer.alloc(0)

    client.publish(action.topic, data, { qos: 1 })

    publishCounter++
    if (publishCounter >= maxCount) {
      clearInterval(interval)
    }
  }, action.rate || 100)
}

function handleSub(client: MqttClient, action: IMqttSubAction) {
  console.log('Subscribing to: ', action.topic)

  client.subscribe(action.topic)
}
