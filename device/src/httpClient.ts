import request from 'request'
import cbor from 'cbor'

import { IHttpConfig, IHttpPostAction } from './config'
import { ActionType } from '.'

export function startHttpClient(config: IHttpConfig) {
  for (const action of config.actions) {
    console.log(
      `${config.scheme}://${config.hostName}:${config.port}/${action.path}`,
    )
    if (action.type === ActionType.HttpPost) {
      handlePost(config, action)
    }
  }
}

function handlePost(config: IHttpConfig, action: IHttpPostAction) {
  const maxCount = action.count || 10
  let messageCounter = 0

  const interval = setInterval(function () {
    const currentCounter = ++messageCounter

    console.log(`Posting [${currentCounter}] to: `, action.path)

    const data = action?.payload
      ? Buffer.from(cbor.encode(action?.payload()).toString('hex'), 'hex')
      : Buffer.alloc(0)

    request(
      {
        url: `${config.scheme}://${config.hostName}:${config.port}/${action.path}`,
        method: 'POST',
        body: data,
        encoding: 'hex',
      },
      (error, response, body) => {
        // const buffer = cbor.decodeFirstSync(
        //   body instanceof Buffer ? body : Buffer.from(body, 'hex'),
        // )

        if (error) {
          console.log(`Post [${currentCounter}] error: `, JSON.stringify(error))
        } else {
          console.log(`Post [${currentCounter}] success: `, {
            // body: buffer,
            body: body.toString('hex'),
            buffer: body,
            status: response.statusCode,
          })
        }
      },
    )

    if (messageCounter >= maxCount) {
      clearInterval(interval)
    }
  }, action.rate || 100)
}
