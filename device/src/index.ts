import unique_id from 'unique-id-key'

import { IHttpConfig, IMqttConfig } from './config'
import { startHttpClient } from './httpClient'
import { startMqttClient } from './mqttClient'

export enum ActionType {
  MqttPub = 'pub',
  MqttSub = 'sub',
  HttpPost = 'post',
}

const mqttConfig: IMqttConfig = {
  scheme: 'mqtt',
  port: parseInt(process.env.MQTT_PORT ?? '1883'),
  clientId:
    process.env.MQTT_CLIENT_ID ?? unique_id.RandomString(11, 'uppercase'),
  hostName: process.env.MQTT_HOST ?? 'localhost',
  actions: [
    {
      type: ActionType.MqttPub,
      topic: 'records',
      count: 100,
      payload: () => ({
        altitude: randomInRange(0, 5000),
        latitude: randomInRange(-180, 180),
        longitude: randomInRange(-180, 180),
        timestamp: Date.now(),
      }),
    },
    {
      type: ActionType.MqttSub,
      topic: 'records',
    },
  ],
}

const httpConfig: IHttpConfig = {
  scheme: 'http',
  port: parseInt(process.env.HTTP_PORT ?? '3000'),
  clientId:
    process.env.HTTP_CLIENT_ID ?? unique_id.RandomString(11, 'uppercase'),
  hostName: process.env.HTTP_HOST ?? 'localhost',
  actions: [
    {
      type: ActionType.HttpPost,
      path: 'records',
      count: 10,
      rate: 1000,
      payload: () => ({
        altitude: randomInRange(0, 5000),
        latitude: randomInRange(-180, 180),
        longitude: randomInRange(-180, 180),
        timestamp: Date.now(),
      }),
    },
  ],
}

;(async function main() {
  if (process.env.PROTOCOL === 'mqtt') {
    await startMqttClient(mqttConfig)
  }
  if (process.env.PROTOCOL === 'http') {
    await startHttpClient(httpConfig)
  }
})()

function randomInRange(min: number, max: number) {
  return Math.random() < 0.5
    ? (1 - Math.random()) * (max - min) + min
    : Math.random() * (max - min) + min
}
