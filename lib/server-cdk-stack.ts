import { RemovalPolicy, Stack, StackProps, aws_ecs_patterns as ecs_patterns, aws_ecs as ecs} from 'aws-cdk-lib';
import { AttributeType, Table } from 'aws-cdk-lib/aws-dynamodb';
import { Vpc } from 'aws-cdk-lib/aws-ec2';
import { Construct } from 'constructs';

export class ServerCdkStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const dynamoTable = new Table(this, 'records', {
      partitionKey: {
        name: 'id',
        type: AttributeType.STRING
      },
      tableName: 'records',
      removalPolicy: RemovalPolicy.DESTROY, 
    });
    const vpc = new Vpc(this, 'VPC', {
      maxAzs:2,
      natGateways:2
    });

    new ecs_patterns.ApplicationLoadBalancedFargateService(this, 'EcsService', {
      vpc,
      taskImageOptions: {
      image: ecs.ContainerImage.fromAsset('records-storage-server'),
      containerPort: 80,
      },
    });

  }
}
