# Records-storage-server

The `cdk.json` file tells the CDK Toolkit how to execute your app.

Repository contains two projects, device simulator in `/device` directory and records-storage-server in corresponding directory supported with dockerfile.
AWS cdk is used to deploy cloudformation to aws server. Make sure you configured your aws account before deploying.

What I learned during the day:

* Got more familiar with AWS services.
* Took a deep dive to CDK and cloudformation.
* Understood that `brew` doesn't provide reliable docker-machine package.
* Tried out Koa framework for HTTP server. 
* Had some fun ¯\ _(ツ)_/¯. 

Application is not *finished*. Koa interface seem to be limited to parse raw data. I need a workaround, or changing framework overall. Persistance and API layers are not fully developed, altho DynamoDB included in CDK. 

Cheers 🍻

## Run commands

 * `yarn build`      compile typescript to js
 * `yarn bootstrap`   watch for changes and compile
 * `yarn deploy`      deploy this stack to your default AWS account/region
