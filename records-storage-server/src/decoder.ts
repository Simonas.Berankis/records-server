import cbor from 'cbor';

export class Decoder {
  decode = (input: string) => {
    const decoded = cbor.decode(input);
    console.log(decoded);
  };
}
