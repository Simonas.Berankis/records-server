import Koa from 'koa';
import Router from 'koa-router';
import { DynamoDBClient, BatchExecuteStatementCommand } from '@aws-sdk/client-dynamodb';
import { Decoder } from './decoder';
import KoaBetterBody from 'koa-better-body';

const myRouter = new Router();
const app = new Koa();
const decoder = new Decoder();

const client = new DynamoDBClient({ region: 'eu-west-1' });
const params = {
  Statements: {
    Record: [
      {
        AttributeName: 'altitude',
        AttributeType: 'N',
      },
      {
        AttributeName: 'latitude',
        AttributeType: 'N',
      },
      {
        AttributeName: 'longitude',
        AttributeType: 'N',
      },
      {
        AttributeName: 'timestamp',
        AttributeType: 'S',
      },
    ],
  },
  TableName: 'records',
};

const options: KoaBetterBody.Options = {
  buffer: true,
};

myRouter.get('/records/:id', async (ctx, next) => {
  ctx.body = 'record';
  await next();
});

myRouter.post('/records', KoaBetterBody(), function (next) {
  console.log(this.request.files);
  console.log(this.request.field);
  console.log(this.request.body);

  next();
});
app.use(myRouter.routes());

console.log('start app');

app.listen(80);
