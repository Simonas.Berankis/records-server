#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { ServerCdkStack } from '../lib/server-cdk-stack';

const app = new cdk.App();
new ServerCdkStack(app, 'ServerCdkStack', {
  // env: { account: "709760132103", region: 'eu-west-1' },
});

app.synth();